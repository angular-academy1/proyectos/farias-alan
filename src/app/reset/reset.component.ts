import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.css'],
})
export class ResetComponent {
  @Input() value!: number;
  @Output() valueChange = new EventEmitter();

  pasar() {
    this.valueChange.emit(this.value);
  }

  reset() {
    this.value = 0;
    this.valueChange.emit(0);
  }
}
