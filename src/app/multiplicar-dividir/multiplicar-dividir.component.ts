import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-multiplicar-dividir',
  templateUrl: './multiplicar-dividir.component.html',
  styleUrls: ['./multiplicar-dividir.component.css'],
})
export class MultiplicarDividirComponent implements OnInit {
  @Input() value!: number;
  @Output() valueChange = new EventEmitter();

  pasar() {
    this.valueChange.emit(this.value);
  }
  mult() {
    this.value = this.value * 2;
    this.valueChange.emit(this.value);
  }
  div() {
    this.value = this.value / 2;
    this.valueChange.emit(this.value);
  }
  ngOnInit(): void {}
}
