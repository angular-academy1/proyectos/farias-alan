import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ResetComponent } from './reset/reset.component';
import { IncrementarDecrementarComponent } from './incrementar-decrementar/incrementar-decrementar.component';
import { MultiplicarDividirComponent } from './multiplicar-dividir/multiplicar-dividir.component';

@NgModule({
  declarations: [
    AppComponent,
    ResetComponent,
    IncrementarDecrementarComponent,
    MultiplicarDividirComponent,
  ],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
