import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-incrementar-decrementar',
  templateUrl: './incrementar-decrementar.component.html',
  styleUrls: ['./incrementar-decrementar.component.css'],
})
export class IncrementarDecrementarComponent implements OnInit {
  @Input() value!: number;
  @Output() valueChange = new EventEmitter();

  pasar() {
    this.valueChange.emit(this.value);
  }
  inc() {
    this.value++;
    this.valueChange.emit(this.value);
  }
  dec() {
    this.value--;
    this.valueChange.emit(this.value);
  }

  ngOnInit(): void {}
}
